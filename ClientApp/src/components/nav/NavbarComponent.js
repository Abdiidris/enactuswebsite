import './Navbar.css';
import React, { Component } from 'react';

class NavbarComponent extends Component {
    render() {
        return (
           
            <div id="navBar" class="row">
                <ul class="col-sm-12">
                    <div class="logo"></div>
                    <button class="button">Take Action</button>
                    <li><a href="#getinvolved">Get Involved</a></li>
                    <li><a href="#members">Members</a></li>
                    <li><a href="#about">About Us</a></li>
                    <li><a href="#homepage">Home</a></li>
                </ul>
            </div>
        )
    }
}


export default NavbarComponent;