import './Home.css';
import React, { Component } from 'react';
import ReactTypingEffect from 'react-typing-effect';

//import $ from 'jquery/dist/jquery'

import picOne from "./assets/pone.jpg"
import picTwo from "./assets/ptwo.jpg"
import picThree from "./assets/pthree.jpg"

const images = [

    picThree,
    picTwo,
    picOne
];


class HomeComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            image: images[0],
            counter: 0
        }
    }
    componentDidMount() {
        setInterval(
            function () {

                if (this.state.counter == images.length) {
                    this.setState({
                        image: images[0],
                        counter: 0
                    });
                }
                else {
                    this.setState({
                        image: images[this.state.counter],
                        counter: this.state.counter + 1
                    });
                }

            }.bind(this), 5000);

    }
    render() {
        return (
            <div class="container m-0 p-0" id="pages">
                <div id="homePage" class="row page">
                    <div id="imageContainer" class="col">
                        <img id="homeImage" class="img" src={this.state.image} alt="Responsive image" />
                        <h1 id="imageText">
                            <ReactTypingEffect
                                text={["This is Aston uni enactus!", "Here to create change."]}
                                speed={100}
                                eraseDelay={4900}
                            />
                        </h1>
                    </div>
                </div>
                <div id="aboutUs" class="page" >
                    <div class="row" >
                        <div class="col" id="aboutSummary">
                        "At Enactus,we develop our skills, which we utilize towards improving the confidence and skills of the most vulnerable in society. Helping to empower those individuals to make a difference within their lives and others around them. By doing this we are able to help each other and make the world a better place."
                          </div>
                    </div>
                    <div class="row p-2" >
                        <div class="col">
                            <h3>
                                Our goals
                            </h3>

                        </div>
                    </div>
                    <div id="unGoals" class="row bg-white" >
                        <div class="col-sm-1">
                            B
                        </div>
                        <div class="col-sm-2 bg-primary cardContainer">
                            <div class="card text-black  unGoalCards">
                                <div class="card-header">Header</div>
                                <div class="card-body">
                                    <h5 class="card-title">Primary card title</h5>
                                    <p class="card-text">n the card title and make up the bulk of the card's content.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2 cardContainer">
                            <div class="card text-black mb-3 unGoalCards" >
                                <div class="card-header">Header</div>
                                <div class="card-body">
                                    <h5 class="card-title">Primary card title</h5>
                                    <p class="card-text">title and make up the bulk of the card's content.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2 cardContainer">
                            <div class="card text-black mb-3 unGoalCards" >
                                <div class="card-header">Header</div>
                                <div class="card-body">
                                    <h5 class="card-title">Primary card title</h5>
                                    <p class="card-text">he card's content.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2 cardContainer">
                            <div class="card text-black mb-3 unGoalCards">
                                <div class="card-header">Header</div>
                                <div class="card-body">
                                    <h5 class="card-title">Primary card title</h5>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2 bg-primary cardContainer">
                            <div class="card text-black mb-3 unGoalCards">
                                <div class="card-header">Header</div>
                                <div class="card-body">
                                    <h5 class="card-title">Primary card title</h5>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1 cardContainer">
                            F
                        </div>
                    </div>
                    <div id="projects" class="row bg-success" >
                        <div class="col">
                            projects
                        </div>
                    </div>
                </div>

            </div>
            /* 
             <div class="container">
 
                 <div id="home" class="row" >
                     <div class="col">
                         <img id="homeImage" class="img" src={this.state.image} alt="Responsive image" />
                         <h1 id="imageText">
                             <ReactTypingEffect
                                 text={["This is Aston uni enactus!", "Here to create change."]}
                                 speed={100}
                                 eraseDelay={4900}
                             />
                         </h1>
                     </div>
                 </div>
 
                 <div id="cards" class="row">
                     <div class="col-sm">
                         <div class="card" style={{ 'width': '18rem' }}>
                             <img src={picOne} class="card-img-top" alt="..." />
                             <div class="card-body">
                                 <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                             </div>
                         </div>
                     </div>
                 </div>
 
 
             </div>
             */
        )
    }
}


export default HomeComponent;