import 'bootstrap/dist/css/bootstrap.css';

import React from 'react';
import ReactDOM from 'react-dom';
import NavbarComponent from './components/nav/NavbarComponent';
import HomeComponent from './components/home/HomeComponent';

openNavbar();
openHome();
function openHome() {
  ReactDOM.render(
    <HomeComponent />,
    document.getElementById('root')
  );
}


function openNavbar() {
  ReactDOM.render(
    <NavbarComponent />,
    document.getElementById('navBarContainer')
  );
}
